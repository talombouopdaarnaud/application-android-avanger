// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  userProfile: null,
  firebase: {
    apiKey: "AIzaSyCFFyCZw_hjEkG_OvxYqi_HVm-6EHIiSkc",
    authDomain: "kmerconcour.firebaseapp.com",
    databaseURL: "https://kmerconcour.firebaseio.com",
    projectId: "kmerconcour",
    storageBucket: "kmerconcour.appspot.com",
    messagingSenderId: "562797960396",
    appId: "1:562797960396:web:57e6e6c663dfc4e9"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
