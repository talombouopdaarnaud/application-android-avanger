import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlackWidowPage } from './black-widow.page';

describe('BlackWidowPage', () => {
  let component: BlackWidowPage;
  let fixture: ComponentFixture<BlackWidowPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlackWidowPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlackWidowPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
