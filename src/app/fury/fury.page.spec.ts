import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FuryPage } from './fury.page';

describe('FuryPage', () => {
  let component: FuryPage;
  let fixture: ComponentFixture<FuryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FuryPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FuryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
