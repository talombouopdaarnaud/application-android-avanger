import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HulkPage } from './hulk.page';

describe('HulkPage', () => {
  let component: HulkPage;
  let fixture: ComponentFixture<HulkPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HulkPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HulkPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
