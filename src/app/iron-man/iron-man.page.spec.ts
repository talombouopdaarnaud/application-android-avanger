import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IRONMANPage } from './iron-man.page';

describe('IRONMANPage', () => {
  let component: IRONMANPage;
  let fixture: ComponentFixture<IRONMANPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IRONMANPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IRONMANPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
