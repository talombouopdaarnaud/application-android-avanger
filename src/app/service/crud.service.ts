import { Injectable } from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class CrudService {

  constructor(private firestore: AngularFirestore) {
  }
  create_NewStudent(record) {
    return this.firestore.collection('publicite').add(record);
  }

  read_Students() {
    console.log('recupere');
    console.log(this.firestore.collection('publicite').snapshotChanges());
    return this.firestore.collection('publicite').snapshotChanges();
  }

  update_Student(recordID,record){
    this.firestore.doc('Students/' + recordID).update(record);
  }

  delete_Student(record_id) {
    this.firestore.doc('Students/' + record_id).delete();
  }
}
