import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LokyPage } from './loky.page';

describe('LokyPage', () => {
  let component: LokyPage;
  let fixture: ComponentFixture<LokyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LokyPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LokyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
