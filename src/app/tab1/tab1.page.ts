import {Component, OnInit} from '@angular/core';
import {CrudService} from '../service/crud.service';
import {AngularFirestore} from '@angular/fire/firestore';
import {LoadingController, NavController} from '@ionic/angular';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  students: any;
  studentName: string;
  studentAge: number;
  studentAddress: string;

  constructor(private crudService: CrudService,public navCtrl: NavController) {}

  ngOnInit() {
      this.crudService.read_Students();
      this.crudService.read_Students().subscribe(data => {

       this.students = data.map(e => {
          return {
            id: e.payload.doc.id,
            isEdit: false,
            Name: e.payload.doc.data()['titre'],
            Age: e.payload.doc.data()['contenue']
          };
        })
        console.log('dara recupere');
        console.log(this.students);

      });
  }
    //paser a la page sigin
    gopageim(){
        this.navCtrl.navigateForward('iron-man');
    }
    gopagethor(){
        this.navCtrl.navigateForward('thor');
    }
    gopagehulk(){
        this.navCtrl.navigateForward('hulk');
    }
    gopagecaptainamerica(){
        this.navCtrl.navigateForward('captain-america');
    }
    gopagefury(){
        this.navCtrl.navigateForward('fury');
    }
    gopageha(){
        this.navCtrl.navigateForward('ha');
    }
    gopageblackwidow(){
        this.navCtrl.navigateForward('black-widow');
    }
    gopageloky(){
        this.navCtrl.navigateForward('loky');
    }

}
