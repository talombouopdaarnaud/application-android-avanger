import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'iron-man', loadChildren: './iron-man/iron-man.module#IRONMANPageModule' },
  { path: 'thor', loadChildren: './thor/thor.module#ThorPageModule' },
  { path: 'hulk', loadChildren: './hulk/hulk.module#HulkPageModule' },
  { path: 'captain-america', loadChildren: './captain-america/captain-america.module#CaptainAmericaPageModule' },
  { path: 'fury', loadChildren: './fury/fury.module#FuryPageModule' },
  { path: 'ha', loadChildren: './ha/ha.module#HaPageModule' },
  { path: 'black-widow', loadChildren: './black-widow/black-widow.module#BlackWidowPageModule' },
  { path: 'loky', loadChildren: './loky/loky.module#LokyPageModule' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
