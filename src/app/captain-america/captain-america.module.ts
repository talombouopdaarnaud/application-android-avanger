import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CaptainAmericaPage } from './captain-america.page';

const routes: Routes = [
  {
    path: '',
    component: CaptainAmericaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CaptainAmericaPage]
})
export class CaptainAmericaPageModule {}
