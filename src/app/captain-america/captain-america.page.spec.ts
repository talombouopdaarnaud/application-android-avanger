import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaptainAmericaPage } from './captain-america.page';

describe('CaptainAmericaPage', () => {
  let component: CaptainAmericaPage;
  let fixture: ComponentFixture<CaptainAmericaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaptainAmericaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaptainAmericaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
