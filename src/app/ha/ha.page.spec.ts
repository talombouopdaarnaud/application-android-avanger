import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HaPage } from './ha.page';

describe('HaPage', () => {
  let component: HaPage;
  let fixture: ComponentFixture<HaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
